#!/bin/ksh

#Author : Sandeep Rathor

Red='\033[0;31m'
Green='\033[0;32m'
Yellow='\033[0;33m'
Cyan='\033[0;36m'
NC='\033[0m'

printf "\n${Cyan}Calling Subcriber No: ${NC}"
read sSUBSCRIBER
iIMSI=`sqlplus -s ${APP_ORA_USER}/${APP_ORA_PASS}@${APP_DB_INST} << EOF
SET HEAD OFF PAGESIZE 0 LINESIZE 1000 TRIMOUT ON TIMING OFF
SET FEEDBACK OFF
select resource_value from agd1_resources where resource_type='3'
and subscriber_id in (select subscriber_id from agd1_resources where resource_value='$sSUBSCRIBER') and sub_status='A';
exit;
EOF`
if [ -z "$iIMSI" ] ; then
        echo "${Red}IMSI not found in Agd1_resources. Script Terminated !${NC}\n"
        exit;
fi

printf "\n${Cyan}Date(YYYYMMDD): ${NC}"
read iDATE

echo "\n${Cyan}Please Verify your Inputs....... ${NC}"
echo "Calling Subscriber No: ${Yellow}$sSUBSCRIBER${NC}"
echo "IMSI: ${Yellow}$iIMSI${NC}"
echo "Date: ${Yellow}$iDATE${NC}"

printf "\n${Cyan}Fine ? (Y/N): ${NC}"
read VERIFY
if [ "$VERIFY" == "Y" ] ; then
        echo "\n${Green}Script Running.....${NC}"
else
        echo "\n${Red}Script Terminated !${NC}"
        exit;
fi

SEQNO=`sqlplus -s ${APP_ORA_USER}/${APP_ORA_PASS}@${APP_DB_INST} << EOF
SET HEAD OFF PAGESIZE 0 LINESIZE 10000 TRIMOUT ON TIMING OFF
select Last_sequence from smm3_sequences where sequence_name='FILE_ID_SEQENCE';
exit;
EOF`

SEQNO=$(($SEQNO + 1))
if ((SEQNO < 10));
then
        NSEQNO="000"$SEQNO""

elif (( ((SEQNO > 9)) && ((SEQNO < 100)) ));
then
        NSEQNO="00"$SEQNO""

elif (( ((SEQNO > 99)) && ((SEQNO < 1000)) ));
then
        NSEQNO="0"$SEQNO""
else    NSEQNO="$SEQNO"
fi

FILENAME="SMSSG2018032215471900"$NSEQNO""

echo "\nFile Name: ${Green}$FILENAME${NC}"
sed -e "s/MSISDN/$sSUBSCRIBER/g ; s/DATE/$iDATE/g ; s/IMSI/$iIMSI/g"  sandeep/SMS_FULL_FILE.txt > var/mps/projs/up/physical/switch/SMS/$FILENAME
echo "File is placed in var/mps/projs/up/physical/switch/SMS to process...\n"

