#!/bin/ksh

#Author : Sandeep Rathor

Red='\033[0;31m'
Green='\033[0;32m'
Yellow='\033[0;33m'
Cyan='\033[0;36m'
NC='\033[0m'

cat<< END

1. Voice(MSC), MTAS, VPNI
2. 3PB(Fax), VOV, VPNR
3. NGME, SGW
4. SMS, MMS
5. Appender
6. TAPIN
7. TAPOUT
END

printf "\n${Yellow}Piease Enter Your Choice No: ${NC}"
read choice

printf "\n${Cyan}Starting Core...${NC}\n"
${HOME}/ASMM/Core/bin/xacct test -i > ${HOME}/sandeep/myofcalog.txt
if [ $? -eq 10 ];
then
	${HOME}/ASMM/Core/bin/xacct enable
	echo "Please wait.."
        sleep 600
elif [ $? -eq 0 ];then	
echo "Core is alredy started or UP !"
fi

printf  "\n${Cyan}Starting G01 OFCA...${NC}\n"
${HOME}/ASMM/G01_OFCA/bin/xacct enable

case $choice in
               1) 
               printf "\n${Cyan}Starting G02 OFCA...${NC}\n"
		${HOME}/ASMM/G02_OFCA/bin/xacct enable
		break
               ;;
               2)
               printf "\n${Cyan}Starting G03 OFCA...${NC}\n"
        	${HOME}/ASMM/G03_OFCA/bin/xacct enable
               break
               ;;
               3) 
               printf "\n${Cyan}Starting G04 OFCA...${NC}\n"
        	${HOME}/ASMM/G04_OFCA/bin/xacct enable
               break
               ;;
               4)
               printf "\n${Cyan}Starting G05 OFCA...${NC}\n"
        	${HOME}/ASMM/G05_OFCA/bin/xacct enable
               break
               ;;
               5) 
               printf "\n${Cyan}Starting G06 OFCA...${NC}\n"
        	${HOME}/ASMM/G06_OFCA/bin/xacct enable
               break
               ;;
               6)
		printf "\n${Cyan}Starting TAPIN Gatherers...${NC}\n"
               	${HOME}/ASMM/G01_TAPIN/bin/xacct enable
		${HOME}/ASMM/G02_TAPIN/bin/xacct enable
		${HOME}/ASMM/G03_TAPIN/bin/xacct enable
		${HOME}/ASMM/G04_TAPIN/bin/xacct enable
		${HOME}/ASMM/G05_TAPIN/bin/xacct enable
		break
               ;;
		7)
                printf "\n${Cyan}Starting TAPOUT Gatherers...${NC}\n"
                ${HOME}/ASMM/G01_TAPOUT/bin/xacct enable
		${HOME}/ASMM/G02_TAPOUT/bin/xacct enable
		${HOME}/ASMM/G03_TAPOUT/bin/xacct enable
		break
               ;;

		*)
               printf "\n${Red}Incorrect Choice! ${NC}\n"
               echo ""
               ;;




esac
