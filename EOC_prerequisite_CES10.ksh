#!/bin/ksh
#Author : Sandeep Rathor

Red='\033[0;31m'
Green='\033[0;32m'
Yellow='\033[0;33m'
Cyan='\033[0;36m'
NC='\033[0m'

echo "\n${Red}CAUTION ! ${Yellow}Please provide the following details correctly.${NC}"

printf "\n${Cyan}Cycle year(yyyy):${NC}"
read year

printf "\n${Cyan}Cycle month(mm):${NC}"
read month

printf "\n${Cyan}Cycle code:${NC}"
read ccode

Sequence_Number=`sqlplus -s abpapp1/amdocs10!@VFZ13MLP << EOF
SET HEAD OFF PAGESIZE 0 LINESIZE 1000 TRIMOUT ON TIMING OFF
SET FEEDBACK OFF
select cycle_seq_no from Bl1_cycle_control where cycle_year='$year'  and CYCLE_INSTANCE='$month' and cycle_code='$ccode';
exit;
EOF`

if [ -z "$Sequence_Number" ] ; then
        echo "\n${Red}Cycle Sequence Number not found in bl1_cycle_control. Script Terminated !${NC}\n"
        exit;
fi

echo "\n${Yellow}Step 1: Running Undo for $Sequence_Number... ${NC}"

sqlplus -s abpapp1/amdocs10!@VFZ13MLP<<EOF
update bl1_cycle_control set status = 'CL',run_date = null where cycle_seq_no = $Sequence_Number;
update adj1_cycle_state set locked_for_new='N' where cycle_year=$year and cycle_instance=$month;
delete from bl1_cycle_groups where cycle_seq_no = $Sequence_Number;
delete from bl1_cycle_groups_history where cycle_seq_no = $Sequence_Number;
delete from bl1_cycle_customers where cycle_seq_no = $Sequence_Number;
delete from bl1_cyc_payer_pop where cycle_seq_no = $Sequence_Number;
delete from bl1_cycle_errors where cycle_seq_no = $Sequence_Number;
delete from bl1_run_request where population_id = $Sequence_Number;
delete from bl1_tax_item where tax_seq_no in (select tax_seq_no from bl1_tax where cycle_seq_no = $Sequence_Number);
delete from bl1_tax where cycle_seq_no = $Sequence_Number;
delete from bl1_invoice where cycle_seq_no = $Sequence_Number;
delete from bl1_sam_control where cycle_seq_no = $Sequence_Number;
delete from BL1_BILL_FINANCE_ACT WHERE CYCLE_SEQ_NO =$Sequence_Number;
delete from bl1_inv_statement where document_seq_no in (select doc_seq_no from bl1_document where cycle_seq_no = $Sequence_Number);
delete from bl1_bill_statement where document_seq_no in (select doc_seq_no from bl1_document where cycle_seq_no = $Sequence_Number);
delete from bl1_document where cycle_seq_no = $Sequence_Number;
delete from bl1_charge_acc where cycle_seq_no = $Sequence_Number;
delete from bl1_charge_adj where charge_seq_no in (select charge_seq_no from bl1_charge where cycle_seq_no = $Sequence_Number);
delete from bl1_inv_charge_rel where charge_seq_no in (select charge_seq_no from bl1_charge where cycle_seq_no = $Sequence_Number);
delete from bl1_charge where cycle_seq_no = $Sequence_Number;
delete from bl1_prepaid_statement where cycle_seq_no = $Sequence_Number;
delete bl1_tax_item where period_key in(select period_key from bl1_cycle_control where cycle_seq_no = $Sequence_Number);
delete from ac1_control where data_group like '%$Sequence_Number%';
delete from ac1_control_hist where data_group like '%$Sequence_Number%';
delete from bl3_cyc_payer_pop where cyc_seq_no = $Sequence_Number;
delete from BL1_CYCLE_RUN_STATISTICS where cycle_seq_no =$Sequence_Number;
delete from bl1_cycle_groups where cycle_seQ_no = $Sequence_Number;
delete from bl1_group_status where cycle_seQ_no = $Sequence_Number;
delete from BL1_CYC_QA_POP WHERE CYCLE_SEQ_NO = $Sequence_Number;
delete from BL1_CYC_QA_POP_TEMP WHERE CYCLE_SEQ_NO = $Sequence_Number;
update ADJ3_JOBS_INST_CTRL set status = 'N'where status = 'Y';
delete ADJ3_JOBS_INST_CTRL  where map_key = '%$Sequence_Number%';
delete bl1_group_status where cycle_Seq_no =  $Sequence_Number;
delete bl1_request_msg where add_parameters like '%$Sequence_Number%';
delete from BL1_REQUEST_MSG where request_id in (select request_id from bl1_run_request where population_id =$Sequence_Number);
delete bl1_cycle_groups where cycle_Seq_no = $Sequence_Number;
delete from BL1_UNDO_TECH_MARK WHERE CYCLE_SEQ_NO =$Sequence_Number;
delete from BL1_REJECTED_TRX ;
delete from BL1_CUST_TAX_EXMPT  ;
DELETE APR1_EXTR_MONITOR;
DELETE APR1_EOC_RERATE_TASKS;
DELETE APR1_EXTR_CUST_RECOVERY;
DELETE APR1_OP_MAP_EVENTS;
DELETE APR1_ROLL_ACCUM_CTRL;
UPDATE ADJ3_JOBS_INST_CTRL SET MAP_KEY = NULL, STATUS = 'N';
update ac1_control set file_status = 'CO';
update ac1_control_hist set file_status = 'CO';
update bl1_run_request set status = 'FN';
commit;
exit;
EOF
echo "${Green}Step 1: UNDO COMPLETED SUCCESSFULLY !!${NC}"

echo "${Yellow}Step 2: MRO Cleaning UP... ${NC}"
./CleanMRO.ksh
echo "${Green}Step 2: MRO CLEANDED SUCCESSFULLY !!${NC}"

echo "${Yellow}Step 3: Updating bl1_cycle_control, adj1_cycle_state ${NC}"
sqlplus -s abpapp1/amdocs10!@VFZ13MLP<<EOF
update bl1_cycle_control SET status = 'CN', RUN_DATE=END_DATE+2  where cycle_year = $year and cycle_instance < $month ;
update adj1_cycle_state SET LOCKED_FOR_NEW = 'Y',LOCKED_FOR_UPDATE = 'Y' where cycle_year = $year and cycle_instance < $month ;
update bl1_cycle_control SET status = 'CL', RUN_DATE=null where cycle_year = $year and cycle_instance = $month ;
update bl1_cycle_control SET status = 'PN', RUN_DATE=null where cycle_year = $year and cycle_instance > $month ;
update adj1_cycle_state SET LOCKED_FOR_NEW = 'N',LOCKED_FOR_UPDATE = 'N' where cycle_year = $year and cycle_instance >= $month ;
commit;
exit;
EOF
echo "${Green}Step 3: TABLES UPDATED SUCCESSFULLY !!${NC}"

echo "${Yellow}Step 4: Updating Logical date ${NC}"
sqlplus -s abpapp1/amdocs10!@VFZ13MLP<<EOF
update logical_date set logical_date=(SELECT END_DATE FROM BL1_CYCLE_CONTROL  WHERE CYCLE_YEAR ='$year' AND CYCLE_CODE = '$ccode' AND CYCLE_INSTANCE ='$month')+2 ;
commit;
exit;
EOF
echo "${Green}Step 4: LOGICAL DATE UPDATED SUCCESSFULLY !!${NC}"
echo "${Yellow}Step 5: Run REFNOT Job.. ${NC}"
RunJobs UTL1REFNOT BYREQ

