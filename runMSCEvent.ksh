#!/bin/ksh
#Author : Sandeep Rathor

Red='\033[0;31m'
Green='\033[0;32m'
Yellow='\033[0;33m'
Cyan='\033[0;36m'
NC='\033[0m'
printf "\n${Cyan}Calling Number(Starting with 316): ${NC}"
read sSUBSCRIBER
printf "\n${Cyan}Called Number(Starting with 14 or 11): ${NC}"
read dSUBSCRIBER
printf "\n${Cyan}Date & Time(YYYYMMDDhhmmss): ${NC}"
read dTime

year=`echo $dTime | cut -c 1-4`
month=`echo $dTime | cut -c 5-6`
day=`echo $dTime | cut -c 7-8`

hour=`echo $dTime | cut -c 9-10`
if [ "$hour" == "" ]; then
hour="01"
elif (($hour < 10)); then
hour="0"$hour""
fi

minute=`echo $dTime | cut -c 11-12`
if [ "$minute" == "" ]; then
minute="01"
elif (($minute < 10)); then
minute="0"$minute""
fi

second=`echo $dTime | cut -c 13-14`
if [ "$second" == "" ]; then
second="01"
elif (($second < 10)); then
second="0"$second""
fi

iDATE=($year-$month-$day)
iTIME=($hour:$minute:$second)

printf "\n${Cyan}Duration(Seconds): ${NC}"
read iDURATION

iIMSI=`sqlplus -s ${APP_ORA_USER}/${APP_ORA_PASS}@${APP_DB_INST} << EOF
SET HEAD OFF PAGESIZE 0 LINESIZE 1000 TRIMOUT ON TIMING OFF
SET FEEDBACK OFF
select resource_value from agd1_resources where resource_type='3'
and subscriber_id in (select subscriber_id from agd1_resources where resource_value='$sSUBSCRIBER') and sub_status='A' and expiration_date>sysdate;
exit;
EOF`
if [ -z "$iIMSI" ] ; then
        echo "${Red}IMSI not found in Agd1_resources. Script Terminated !${NC}\n"
        exit;
fi

echo "\n${Cyan}Please Verify your Inputs....... ${NC}\n"
echo "Calling Number: ${Yellow}$sSUBSCRIBER${NC}"
echo "IMSI: ${Yellow}$iIMSI${NC}"
echo "Called Number: ${Yellow}$dSUBSCRIBER${NC}"
echo "Date and Time: ${Yellow}$iDATE $iTIME ${NC}"
echo "Duration(Seconds): ${Yellow}$iDURATION${NC}"


printf "\n${Cyan}Fine ? (Y/N): ${NC}"
read VERIFY
if [ "$VERIFY" == "Y" ] ; then
        echo "\n${Green}Script Running......${NC}"
else
        echo "\n${Red}Script Terminated !${NC}"
        exit;
fi

SEQNO=`sqlplus -s ${APP_ORA_USER}/${APP_ORA_PASS}@${APP_DB_INST} << EOF
SET HEAD OFF PAGESIZE 0 LINESIZE 10000 TRIMOUT ON TIMING OFF
select Last_sequence from smm3_sequences where sequence_name='FILE_ID_SEQENCE';
exit;
EOF`
SEQNO=$(($SEQNO + 1))

if ((SEQNO < 10));
then
        NSEQNO="0000"$SEQNO""

elif (( ((SEQNO > 9)) && ((SEQNO < 100)) ));
then
        NSEQNO="000"$SEQNO""

elif (( ((SEQNO > 99)) && ((SEQNO < 1000)) ));
then
        NSEQNO="00"$SEQNO""
elif (( ((SEQNO > 999)) && ((SEQNO < 10000)) ));
then
        NSEQNO="0"$SEQNO""

else    NSEQNO="$SEQNO"
fi

FILENAME="MSC_"$NSEQNO"_20180805131140_YA_6020"

echo "\nFile Name: ${Green}$FILENAME${NC}"

sSUBSCRIBER=${sSUBSCRIBER#*1}
sed -e "s/TONUMBER/$dSUBSCRIBER/g ; s/FROMNUMBER/14$sSUBSCRIBER/g ; s/IMSIVALUE/$iIMSI/g ; s/DATE/$iDATE/g ; s/TIME/$iTIME/g ; s/DURATION/$iDURATION/g"  sandeep/MSC_file.xml > var/mps/projs/up/physical/switch/MSC/$FILENAME.xml
echo "\nFile is placed under var/mps/projs/up/physical/switch/MSC to process...\n"

