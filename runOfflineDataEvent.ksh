#!/bin/ksh

#Author : Sandeep Rathor

Red='\033[0;31m'
Green='\033[0;32m'
Yellow='\033[0;33m'
Cyan='\033[0;36m'
NC='\033[0m'


echo "\n${Cyan}Select Country: ${NC}"
echo "\n${Yellow} 1)Netherlands(National) \n 2)Belgium(EU) \n 3)Germany(EU) \n 4)India(ROW) \n 5)USA(ROW) \n 6)CUBA(Exception) \n 7)Turkey\n 8)SGSN ADDRESS(Manual)${NC}"

read choice
case $choice in
1)
MYADD=196.012.201.089
;;
2)
MYADD=213.181.060.001
;;
3)
MYADD=193.254.136.001
;;
4)
MYADD=202.159.213.049
;;
5)
MYADD=066.001.065.012
;;
6)
MYADD=200.013.145.065
;;
7)
MYADD=212.065.133.033
;;
8)
printf "\n${Cyan}PLease Enter SGSN Address(Ex: 177.079.244.000): ${NC}"
read MYADD
;;
*)
echo "\n${Red}Sorry ! There is something wrong with your imput....${NC}"
;;
esac

printf "\n${Cyan}IMSI: ${NC}"
read IMSI

printf "\n${Cyan}Data Amount(KB): ${NC}"
read AMOUNT
let "BYTE = ( $AMOUNT / 2.0 ) * 1024"

printf "\n${Cyan}Date & Time(YYYYMMDDHHMMSS): ${NC}"
read TIME

echo "\n${Cyan}Please Verify your Inputs.... ${NC}"
echo "\nIMSI: ${Yellow}$IMSI${NC}"
echo "SGSN Address: ${Yellow}$MYADD${NC}"
eco "Data Amount(KB): ${Yellow}$AMOUNT${NC}"
echo "Date and Time: ${Yellow}$TIME${NC}"

printf "\n${Cyan}Fine ? (Y/N): ${NC}"
read VERIFY
if [ "$VERIFY" != "Y" ] ; then
	echo "\n${Red}Script Terminated !${NC}"
	exit;
fi
SEQNO=`sqlplus -s ${APP_ORA_USER}/${APP_ORA_PASS}@${APP_DB_INST} << EOF
SET HEAD OFF PAGESIZE 0 LINESIZE 10000 TRIMOUT ON TIMING OFF
select Last_sequence from smm3_sequences where sequence_name='FILE_ID_SEQENCE';
exit;
EOF`
SEQNO=$(($SEQNO + 1))

if ((SEQNO < 10));
then
        NSEQNO="00000"$SEQNO""

elif (( ((SEQNO > 9)) && ((SEQNO < 100)) ));
then
        NSEQNO="0000"$SEQNO""

elif (( ((SEQNO > 99)) && ((SEQNO < 1000)) ));
then
        NSEQNO="000"$SEQNO""
elif (( ((SEQNO > 999)) && ((SEQNO < 10000)) ));
then
        NSEQNO="00"$SEQNO""
elif (( ((SEQNO > 9999)) && ((SEQNO < 100000)) ));
then
        NSEQNO="0"$SEQNO""

else    NSEQNO="$SEQNO"
fi

FILENAME="NGMEX120180621113003"$NSEQNO""

echo "\nFile Name: ${Green}$FILENAME${NC}"

sed -e "s/IMSIVALUE/$IMSI/g ; s/DTIME/$TIME/g ; s/DATAVALUE/$BYTE/g ; s/SGSNVALUE/$MYADD/g"  sandeep/NGME_file.xml > var/mps/projs/up/physical/switch/NGME/$FILENAME
cd var/mps/projs/up/physical/switch/NGME
gzip $FILENAME
echo "\nFile is placed under var/mps/projs/up/physical/switch/NGME to process...\n"
