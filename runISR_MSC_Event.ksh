#!/bin/ksh
#Author : Sandeep Rathor

Red='\033[0;31m'
Green='\033[0;32m'
Yellow='\033[0;33m'
Cyan='\033[0;36m'
NC='\033[0m'
printf "\n${Cyan}IMSI: ${NC}"
read iIMSI
printf "\n${Cyan}Called NO: ${NC}"
read iCALLED_NO

printf "\n${Cyan}Date & Time(YYYYMMDDhhmmss): ${NC}"
read dTime

year=`echo $dTime | cut -c 1-4`
month=`echo $dTime | cut -c 5-6`
day=`echo $dTime | cut -c 7-8`

hour=`echo $dTime | cut -c 9-10`
if [ "$hour" == "" ]; then
hour="01"
fi

minute=`echo $dTime | cut -c 11-12`
if [ "$minute" == "" ]; then
minute="01"
fi

second=`echo $dTime | cut -c 13-14`
if [ "$second" == "" ]; then
second="01"
fi

iDATE=($year-$month-$day)
iTIME=($hour:$minute:$second)


echo "\n${Cyan}Please Verify your Inputs....... ${NC}\n"
echo "IMSI: ${Yellow}$iIMSI${NC}"
echo "Date and Time: ${Yellow}$iDATE $iTIME ${NC}"

printf "\n${Cyan}Fine ? (Y/N): ${NC}"
read VERIFY
if [ "$VERIFY" == "Y" ] ; then
        echo "\n${Green}Script Running......${NC}"
else
        echo "\n${Red}Script Terminated !${NC}"
        exit;
fi

SEQNO=`sqlplus -s ${APP_ORA_USER}/${APP_ORA_PASS}@${APP_DB_INST} << EOF
SET HEAD OFF PAGESIZE 0 LINESIZE 10000 TRIMOUT ON TIMING OFF
select Last_sequence from smm3_sequences where sequence_name='FILE_ID_SEQENCE';
exit;
EOF`
SEQNO=$(($SEQNO + 1))

if ((SEQNO < 10));
then
        NSEQNO="0000"$SEQNO""

elif (( ((SEQNO > 9)) && ((SEQNO < 100)) ));
then
        NSEQNO="000"$SEQNO""

elif (( ((SEQNO > 99)) && ((SEQNO < 1000)) ));
then
        NSEQNO="00"$SEQNO""
elif (( ((SEQNO > 999)) && ((SEQNO < 10000)) ));
then
        NSEQNO="0"$SEQNO""

else    NSEQNO="$SEQNO"
fi


FILENAME="MSC_"$NSEQNO"_20180805131140_YA_6020"

echo "\nFile Name: ${Green}$FILENAME${NC}"

sed -e "s/IMSI_VALUE/$iIMSI/g ; s/DATE_VALUE/$iDATE/g ; s/TIME_VALUE/$iTIME/g ; s/CALLED_NO/$iCALLED_NO/g"  sandeep/ISR_VOICE_file.xml > var/mps/projs/up/physical/switch/MSC/$FILENAME.xml
echo "\nFile is placed under var/mps/projs/up/physical/switch/MSC to process...\n"
