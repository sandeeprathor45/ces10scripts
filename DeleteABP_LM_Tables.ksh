
#!/bin/ksh
# Created By: Sandeep Rathor
echo "************Checking ABP Server Status******************"

APP_DB_USER=${_PREFIX_U}APP${ENV_NUM}
APP_DB_PASS=${_PREFIX_U}APP${ENV_NUM}
APP_DB_INST=${_OP_ORA_INST}

ABP_WL_STATUS=$($HOME/JEE/scripts/pingServer)
if [ "$ABP_WL_STATUS" == "UP" ]
then
echo " ABP SERVER IS UP.. PLEASE STOP THE SERVER FIRST"
exit 1

elif [ "$ABP_WL_STATUS" == "DOWN" ];
then
echo "ABP SERVER IS DOWN.. CONTINUING WITH CLEANING OF ABP LM TABLES"

echo "CONNECTING TO ABP DATABASE"
sqlplus -s $APP_ORA_PASS/$APP_ORA_USER@$APP_DB_INST<<EOF
delete TABLE_BPM_LM_SERVER;
delete TABLE_BPM_LM_CONNECTION;
delete from TRB1_AMC_HISTORY;
delete from trb1_audit_interval;
delete from TRB1_ENG_CNTRL;
delete from TRB1_ERR_DEPENDENT;
delete from TRB1_IMP_PERIOD;
delete from TRB1_MEMBER_ADMIN;
delete from TRB1_MONITOR_INFO;
delete from TRB1_QUE_CNTRL;
delete from TRB1_RECV_DATA;
delete from TRB1_SUB_APPL_CNTRL;
delete from TRB1_THR_CNTRL;
delete from TRB1_USER_CONFIG;
delete from TRB1_SUB_APPL_CNTRL;
delete UTL1_DAEMON_LOCKING  where DAEMON_CODE = 'MSD';
commit;
exit;
EOF

echo "Tables Cleaned !!"
fi
