#!/bin/ksh

#Author : Sandeep Rathor

Red='\033[0;31m'
Green='\033[0;32m'
Yellow='\033[0;33m'
Cyan='\033[0;36m'
NC='\033[0m'


printf "\n${Cyan}PLease Enter APN NAME: ${NC}"
read MYAPN
printf "\n${Cyan}IMSI: ${NC}"
read MYIMSI
printf "\n${Cyan}Date & Time(YYYYMMDDHHMMSS): ${NC}"
read TIME

VAR=`sqlplus -s ${APP_ORA_USER}/${APP_ORA_PASS}@${APP_DB_INST} << EOF
SET HEAD OFF
SET VERIFY OFF
SET FEEDBACK OFF
SELECT rule_base || ';' || rating_group FROM ( SELECT * FROM PC9_RATING_GRP_2_SRV_CD ORDER BY DBMS_RANDOM.VALUE ) where APN='$MYAPN'  and rownum = 1;
exit;
EOF`

MYRB=$(echo $VAR | awk -F ';' '{print $1}')
MYRTG=$(echo $VAR | awk -F ';' '{print $2}')

if [ -z "$VAR" ] ; then
        echo "${Red}APN Name not found in PC9_RATING_GRP_2_SRV. Script Terminated !${NC}\n"
        exit;
fi

echo "\n${Cyan}Please Verify your Inputs.... ${NC}"
echo "\nAPN name: ${Yellow}$MYAPN${NC}"
echo "Rule Base: ${Yellow}$MYRB${NC}"
echo "Rating group: ${Yellow}$MYRTG${NC}"
echo "IMSI: ${Yellow}$MYIMSI${NC}"
echo "Date and Time: ${Yellow}$TIME${NC}"

printf "\n${Cyan}Fine ? (Y/N): ${NC}"
read VERIFY
if [ "$VERIFY" != "Y" ] ; then
	echo "\n${Red}Script Terminated !${NC}"
	exit;
fi

SEQNO=`sqlplus -s ${APP_ORA_USER}/${APP_ORA_PASS}@${APP_DB_INST} << EOF
SET HEAD OFF PAGESIZE 0 LINESIZE 10000 TRIMOUT ON TIMING OFF
select Last_sequence from smm3_sequences where sequence_name='FILE_ID_SEQENCE';
exit;
EOF`
SEQNO=$(($SEQNO + 1))

if ((SEQNO < 10));
then
        NSEQNO="00000"$SEQNO""

elif (( ((SEQNO > 9)) && ((SEQNO < 100)) ));
then
        NSEQNO="0000"$SEQNO""

elif (( ((SEQNO > 99)) && ((SEQNO < 1000)) ));
then
        NSEQNO="000"$SEQNO""
elif (( ((SEQNO > 999)) && ((SEQNO < 10000)) ));
then
        NSEQNO="00"$SEQNO""
elif (( ((SEQNO > 9999)) && ((SEQNO < 100000)) ));
then
        NSEQNO="0"$SEQNO""

else    NSEQNO="$SEQNO"
fi

FILENAME="NGMEX120180621113003"$NSEQNO""

echo "\nFile Name: ${Green}$FILENAME${NC}"

sed -e "s/APN_NAME/$MYAPN/g ; s/RULE_NAME/$MYRB/g ; s/RTGROUP/$MYRTG/g ; s/IMSI_VALUE/$MYIMSI/g ; s/DTIME/$TIME/g"  sandeep/APN_file.xml > var/mps/projs/up/physical/switch/NGME/$FILENAME
cd var/mps/projs/up/physical/switch/NGME
gzip $FILENAME
echo "\nFile is placed under var/mps/projs/up/physical/switch/NGME to process...\n"
