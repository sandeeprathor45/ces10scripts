#!/bin/ksh

#Author : Sandeep Rathor

Red='\033[0;31m'
Green='\033[0;32m'
Yellow='\033[0;33m'
Cyan='\033[0;36m'
NC='\033[0m'

cat<< END

[1] MSC Event 
[2] SMS Event
[3] GPRS Event(Online)
[4] GPRS Event(Offline)
[5] Sy Data Event
[6] VOV Event
[7] ISR MSC Event

For new LineUP-
[8] MSC Events
[9] SMS Events
[10]VOV Events

Functional Test-
[11] New APN
[12] New SGSN Address
[13] New ISR IMSI Range
END

printf "\n${Cyan}Please Enter your Choice No: ${Yellow}"
read choice
case $choice in
                1)
                        echo "\n${Green}You have selected MSC Event to run !${NC}"
                	runMSCEvent.ksh
			break                
			;;
                2)   	echo "\n${Green}You have selected SMS Event to run!${NC}"
                	runSMSEvent.ksh
                        break
                	;;
                3)     	echo "\n${Green}You have selected GPRS Event(Online) to run!${NC}"
                	./runOnlineDataEvent.ksh
                        break
                	;;
		4|12)   echo "\n${Green}You have selected GPRS Event(Offline) to run!${NC}"
                        ./runOfflineDataEvent.ksh
                        break
                        ;;
		5)      echo "\n${Green}You have selected Sy Data Event to run!${NC}"
                        ./runSyDataEvent.ksh
                        break
                        ;;
                6)    	echo "\n${Green}You have selected VOV Event to run!${NC}"
                	./runVOVEvent.ksh
                        break
                        ;;
	        7|13)   echo "\n${Green}You have selected ISR MSC Event to run!${NC}"
                        ./runISR_MSC_Event.ksh
                        break
                        ;;
                8)      echo "\n${Green}You have selected MSC Events(10 diff Calls) to run !${NC}"
                        runMSCEvent_ALL.ksh
                        break
                        ;;
		9)      echo "\n${Green}You have selected SMS Events(36 diff records) to run!${NC}"
                        runSMSEvent_ALL.ksh
                        break
                        ;;
		10)     echo "\n${Green}You have selected VOV Events(8 diff calls) to run!${NC}"
                        runVOVEvent_ALL.ksh
                        break
                        ;;
		11)     echo "\n${Green}You have selected APN test script to run!${NC}"
                        runAPNtest.ksh
                        break
                        ;;
                *)      echo "\n${Red}Incorrect Choice!${NC}"
                        echo ""
                        ;;
        esac
